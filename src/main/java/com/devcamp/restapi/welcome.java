package com.devcamp.restapi;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class welcome {
     @CrossOrigin
     @GetMapping("/devcamp-welcome")
     public String nice(){
          DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
          Date now = new Date();
          return String.format("Have a nice day. It is %s!.", dateFormat.format(now));
     }     
}
